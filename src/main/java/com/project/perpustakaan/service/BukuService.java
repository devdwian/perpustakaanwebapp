package com.project.perpustakaan.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.perpustakaan.entity.Buku;
import com.project.perpustakaan.repository.BukuRepository;

@Service
public class BukuService {
	@Autowired
	BukuRepository bukuRepository;

	public List<Buku> listAll() {
		return bukuRepository.findAll();
	}

	public Buku getById(long id) {
		return bukuRepository.findById(id).get();
	}

	public void save(Buku buku) {
		bukuRepository.save(buku);
	}
}
