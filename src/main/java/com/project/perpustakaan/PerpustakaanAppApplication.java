package com.project.perpustakaan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PerpustakaanAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(PerpustakaanAppApplication.class, args);
	}

}
