package com.project.perpustakaan.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.perpustakaan.entity.Buku;

public interface BukuRepository extends JpaRepository<Buku, Long> {

}
