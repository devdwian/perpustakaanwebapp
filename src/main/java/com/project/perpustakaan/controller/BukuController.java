package com.project.perpustakaan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.project.perpustakaan.entity.Buku;
import com.project.perpustakaan.service.BukuService;

@Controller
public class BukuController {
	@Autowired
	private BukuService bukuService;

	@RequestMapping("/")
	public String viewHomePage(Model model) {
		List<Buku> listBuku = bukuService.listAll();
		model.addAttribute("listBuku", listBuku);
		return "index";
	}

	@RequestMapping("/buku/{id}")
	public ResponseEntity<Buku> getDataById(@PathVariable(name = "id") long id) {
		Buku buku = bukuService.getById(id);
		return ResponseEntity.ok().body(buku);
	}

	@RequestMapping(value = "/buku", method = RequestMethod.POST)
	public String save(@ModelAttribute("buku") Buku buku) {
		bukuService.save(buku);
		return "redirect:/";
	}
}
