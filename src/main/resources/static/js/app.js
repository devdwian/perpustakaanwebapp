
$('.btn_add').on('click', function () {
	$('#exampleModalLabel').html("Form Add Buku");
	$('.modal-body #id').val(0);
    $('.modal-body #nama').val("");
});

$('table').on('click', '.btn_edit', function () {
	$('#exampleModalLabel').html("Form Edit Buku");
	let id = this.id;
	
	$.ajax({
        url: 'http://localhost:8080/buku/'+id,
        dataType: 'json',
        type: 'get',
        success: function (buku) {
            $('.modal-body #id').val(buku.id);
            $('.modal-body #nama').val(buku.nama);
        }
    });
	
});